# Command Reference for create-hapibot-app


Here's what's available in our cli.

| `create-hapibot-app`| provides the...                                    |
| ------------------- | -------------------------------------------------- |
| **myapp**           | creation of a project `myapp` based in a template  |
| **list**            | List all available templates                       |
| **config**          | walk through on how to edit your config.json file  |
