// This is a way to edit config file using the cli
// corret way: hapi-cli config --url=https://example.com --token=BDDA_123456 --id=123456
// can be used as hapi-cli config | in this way it will ask you for input
let rawUrl
let rawToken
let rawId
let jsonUrl
let verifySpinner

const command = {
  name: 'config',
  run: async toolbox => {
    const { print, filesystem, parameters, question } = toolbox

    if (parameters.options.url === undefined) {
      // ask for information
      rawUrl = await question.input('Please insert your url: ')
      rawToken = await question.input('Please insert your token: ')
      rawId = await question.input(
        'Please type your config repo id, or leave it blank if you are not using extra packages :)'
      )

      verifySpinner = print.spin('Verifying file location...')
      const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
      await filesystem.file(path) // create dir if not exists

      jsonUrl = {
        url: rawUrl,
        token: rawToken,
        id: rawId
      }

      filesystem.write(path, jsonUrl)
      verifySpinner.stop()
      verifySpinner.succeed('Config file was updated!')
    } else {
      verifySpinner = print.spin('Verifying file location...')
      const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
      await filesystem.file(path) // create dir if not exists

      filesystem.write(path, parameters.options)
      verifySpinner.stop()
      verifySpinner.succeed('Config file was updated!')
    }
  }
}

module.exports = command
