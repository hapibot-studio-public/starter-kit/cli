/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable global-require */

let i
let templateChoice
let url
let configUrl

/* istanbul ignore next */
const command = {
  name: 'create-hapibot-app',
  alias: 'manel',
  run: async toolbox => {
    const {
      api,
      filesystem,
      print,
      verify,
      question,
      action,
      general,
      parameters
    } = toolbox

    if (parameters.options.buildTest === true) {
      return
    }

    print.info('')

    // logo hapibot
    const logo = require('fs').readFileSync(
      require('path').join(__dirname, '../assets/logo.art'),
      'utf8'
    )
    print.info(logo)

    // Verify if we have app name or we must ask for it
    toolbox.print.info('Create new app:')
    let projectName
    if (process.argv.length < 3) {
      projectName = await question.input('Insert project name')
    } else {
      projectName = await process.argv.slice(2)
    }
    projectName = projectName.toString()

    // startUp Process, do some verification, and check config file has the info needed.
    await verify.connection()
    const exist = await verify.folderExists(projectName)
    if (exist) process.exit(1)
    await verify.git()
    const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
    await filesystem.file(path) // create dir if not exists
    // await verify.hasTokenAndUrl()
    await verify.hasUrl()
    await verify.hasId()
    await verify.hasToken()

    // Connect to the template group and get all info using gitlab api
    const localConfigData = await filesystem.read(path, 'json')
    const projects = await api.get(localConfigData.url, localConfigData.token)

    const templateList = [] // all info we need to procede
    const names = [] // list with only the name to use as prompt question
    for (i in projects) {
      templateList.push([
        projects[i].name,
        projects[i].http_url_to_repo,
        projects[i].id
      ])
      names.push(projects[i].name)
    }

    // Ask which template user want
    templateChoice = await question.select(names)

    for (i in templateList) {
      if (templateChoice === templateList[i][0]) {
        url = [templateList[i][1], templateList[i][2]]
      }
    }

    configUrl = `https://gitlab.com/api/v4/projects/${url[1]}/repository/files/config.json/raw?ref=master`
    const configJson = await toolbox.api.get(configUrl, localConfigData.token)

    print.success('Project Description:')
    print.info('')
    print.warning(configJson.Description)
    print.info('')
    print.success(`This Template is for ${configJson.Language}`)
    print.info('')

    const answer = await question.sure()
    let answerLoop
    if (answer === 'No') {
      answerLoop = answer
      while (answerLoop === 'No') {
        // back to template choice
        templateChoice = await question.select(names)

        for (i in templateList) {
          if (templateChoice === templateList[i][0]) {
            url = [templateList[i][1], templateList[i][2]]
          }
        }

        configUrl = `https://gitlab.com/api/v4/projects/${url[1]}/repository/files/config.json/raw?ref=master`
        await toolbox.api.get(configUrl, localConfigData.token)

        answerLoop = await question.sure()
      }
    }

    if (localConfigData.id !== '') {
      // ID is not empty, ask and get extra packages
      await action.installExtraPkgs(url, localConfigData, projectName)
    }

    const spinner = toolbox.print.spin('Cloning your template...')
    await action.clone(url[0], projectName, false) // URL[0] is the name of the selected template
    await action.setupGit(projectName, false)

    // change package.json name
    await general.setProjectName(projectName.toString())

    spinner.succeed('Template was clonned!')
    spinner.stop()
    print.success('Your project is ready!')
    print.warning(`type: cd ${projectName}`)
    print.success('Have a good work :)')

    process.exit(0)
  }
}

module.exports = command
