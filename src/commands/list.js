/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
// This command list all templates avaliable in the repository
let prjNameList
let i

const command = {
  name: 'list',
  alias: 'l',
  run: async toolbox => {
    const { api, filesystem, print, parameters } = toolbox
    let path
    // if (parameters.options.path === undefined) {
    path = `${await toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
    // } else {
    // path = parameters.options.path
    // }

    const configJson = await filesystem.read(path, 'json')
    const data = await api.get(configJson.url, configJson.token)

    prjNameList = []
    for (i in data) {
      prjNameList.push(data[i].name)
    }
    print.success('Here is the list of templates that you have: ')
    for (i in prjNameList) {
      print.warning(`-> ${prjNameList[i]}`)
    }
  }
}

module.exports = command
