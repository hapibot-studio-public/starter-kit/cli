/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
module.exports = toolbox => {
  const { print, http } = toolbox
  toolbox.api = {
    get: async (url, token) => {
      const api = http.create({
        baseURL: url,
        headers: {
          'Cache-Control': 'no-cache',
          'PRIVATE-TOKEN': token
        }
      })
      const { ok, data } = await api.get()
      if (ok) {
        return data
      }
      if (data.message === '404 File Not Found') {
        print.error('Could not found the file we are trying to access: ')
        print.warning(`This was the url: ${url}`)
      } else {
        print.error('Could not access api, is this url ok ?')
        print.warning(url)
      }
      print.error('Stoping...')
      process.exit(1)
    }
  }
}
