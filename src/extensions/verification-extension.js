/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-extraneous-dependencies */
const execa = require('execa')

let jsonConfig

module.exports = async toolbox => {
  const { print, filesystem, question, http } = toolbox
  toolbox.verify = {
    git: async () => {
      const spinner = toolbox.print.spin('Verifying Git...')
      const subprocess = execa('git', ['--version'])
      ;(async () => {
        // Catching an error
        try {
          await subprocess
        } catch (error) {
          /* istanbul ignore next */
          subprocess.stdout.pipe(process.stdout)
          /* istanbul ignore next */
          spinner.stop()
          /* istanbul ignore next */
          spinner.fail('You have to install Git')
          /* istanbul ignore next */
          process.exit(1)
        }
      })()

      spinner.stop()
      spinner.succeed('Git installed!')
      return true
    },
    connection: async () => {
      const spinner = toolbox.print.spin('Verifying connection...')
      const conn = http.create({
        baseURL: 'https://google.com',
        headers: {
          Accept: 'application/vnd.github.v3+json'
        }
      })
      const { ok } = await conn.get()
      if (ok) {
        spinner.stop()
        spinner.succeed('Connected to internet!')
        return true
      }
      /* istanbul ignore next */
      spinner.stop()
      /* istanbul ignore next */
      spinner.fail('No internet conection')
      /* istanbul ignore next */
      process.exit(1)
    },
    folderExists: async projectName => {
      const spinner = toolbox.print.spin(
        "Verifying if project's name is available..."
      )
      const path = `./${projectName}`
      const exist = await filesystem.exists(path)
      if (exist === 'dir') {
        spinner.stop()
        spinner.fail("There's already a project with that name")
        return true
      }
      if (exist === 'file') {
        spinner.stop()
        spinner.fail("There's already a file with that name")
        return true
      }
      if (exist === false) {
        spinner.stop()
        spinner.succeed('Good project name!')
        return false
      }
      /* istanbul ignore next */
      spinner.stop()
      /* istanbul ignore next */
      spinner.fail("There's already something with that name")
      /* istanbul ignore next */
      return true
    },
    hasUrl: async () => {
      const spinner = print.spin('Verifying if you have url')
      const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
      await filesystem.file(path)
      const data = await filesystem.read(path)

      if (data !== '') {
        // File not empty
        const jsonData = JSON.parse(data)
        if (jsonData.url !== '') {
          spinner.stop()
          spinner.succeed('You have a url :)')
          return true
        }
        spinner.stop()
        spinner.fail("You dont have a url, let's fix this :)")
        const rawUrl = await question.input('Please insert your URL: ')
        let valueOfToken = jsonData.token
        if (valueOfToken === undefined) {
          valueOfToken = ''
        }
        jsonConfig = {
          url: rawUrl,
          token: valueOfToken
        }
        filesystem.write(path, jsonConfig)
        return true
      }
      // File is empty
      spinner.stop()
      spinner.fail("You dont have a url, let's fix this :)")
      const rawUrl = await question.input('Please insert your URL: ')
      jsonConfig = {
        url: rawUrl,
        token: ''
      }
      filesystem.write(path, jsonConfig)
      return 0
    },
    hasId: async () => {
      const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
      const data = await filesystem.read(path, 'json')

      if (data.id === undefined) {
        print.warning("You don't have a id for custom packages.")
        const inputId = await question.input(
          'Please insert your id or leave it blank:'
        )

        jsonConfig = {
          url: data.url,
          token: data.token,
          id: inputId
        }

        filesystem.write(path, jsonConfig)
        return true
      }
    },
    hasToken: async () => {
      const path = `${toolbox.filesystem.homedir()}/.create-hapibot-app/config.json`
      const data = await filesystem.read(path, 'json')

      if (data.token === '' || data.token === undefined) {
        const ok = await question.confirm(
          "You don't have a token configured, " +
            'this is needed only if you are using private templates, ' +
            'do you need one ?'
        )
        if (ok === true) {
          print.warning(
            'See how to get the token on gitlab: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html'
          )
          print.error('Your token must have access to api and read_repository')
          const token = await question.input('Please insert your token:')
          const newJson = {
            url: data.url,
            token: token,
            id: data.id
          }
          filesystem.write(path, newJson)
          return true
        } else {
          return false
        }
      }
      return false
    }
  }
}
