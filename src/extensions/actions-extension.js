/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-extraneous-dependencies */
const execa = require('execa')

let choosenPackages
let i
let j
let undoSpinner
let removedSimple

module.exports = async toolbox => {
  const { print, filesystem } = toolbox
  // eslint-disable-next-line no-param-reassign
  toolbox.action = {
    exec: async (name, cmd) => {
      const arrCmd = cmd.split(' ')
      const command = arrCmd[0]
      arrCmd.shift()
      process.chdir(`./${name}`)
      await execa(command, arrCmd, {
        stdio: 'inherit'
      })
      process.chdir('..')
      return 0
    },
    clone: async (url, name) => {
      const args = ['clone', url, name]
      await execa('git', args, { stdio: 'inherit' })
      return 0
    },
    setupGit: async name => {
      await filesystem.remove(`./${name}/.git`)
      await execa('git', ['init', `./${name}`])
      return 0
    },
    undo: async name => {
      await toolbox.filesystem.remove(`./${name}`)
    },

    add: async (projectName, filePath, content) => {
      removedSimple = await toolbox.general.replaceAll(content, "'", '"')
      const path = `./${projectName}/${filePath}`
      let data = filesystem.read(path)
      if (
        (await toolbox.general.isJson(data)) &&
        (await toolbox.general.isJson(removedSimple))
      ) {
        data = JSON.parse(data)
        removedSimple = JSON.parse(removedSimple)
        const newFile = {
          ...data,
          ...removedSimple
        }
        await filesystem.write(path, newFile)
        return 'add'
      } else {
        await toolbox.patching.append(path, content)
        return 'append'
      }
    },

    remove: async (projectName, filePath, content) => {
      const path = `./${projectName}/${filePath}`
      await toolbox.patching.replace(path, content, '')
    },
    replace: async (projectName, filePath, content, newContent) => {
      const path = `./${projectName}/${filePath}`
      await toolbox.patching.replace(path, content, newContent)
    },

    /* istanbul ignore next */
    installExtraPkgs: async (url, localConfigData, projectName) => {
      const spin = print.spin('Getting info from template...')
      const configUrl = `https://gitlab.com/api/v4/projects/${url[1]}/repository/files/config.json/raw?ref=master`
      const configJson = await toolbox.api.get(configUrl, localConfigData.token)

      spin.stop()
      spin.succeed('Got it :)')

      const packageNames = []
      const versions = []
      for (i in configJson.packages) {
        packageNames.push(configJson.packages[i].name)
        versions.push(configJson.packages[i].version)
      }

      if (packageNames.length >= 1) {
        choosenPackages = await toolbox.question.checkBoxQuestion(packageNames)
      }

      const choosenVersions = []
      for (i in configJson.packages) {
        for (j in choosenPackages) {
          if (configJson.packages[i].name === choosenPackages[j]) {
            choosenVersions[j] = configJson.packages[i].version
          }
        }
      }
      // URL[0] is the name of the selected template
      await toolbox.action.clone(url[0], projectName, false)
      await toolbox.action.setupGit(projectName, false)

      // change package.json name
      await toolbox.general.setProjectName(projectName.toString())

      /* istanbul ignore next */
      for (i in choosenPackages) {
        const downloadFile = `https://gitlab.com/api/v4/projects/${localConfigData.id}/repository/files/${choosenPackages[i]}.json/raw?ref=master`
        const data = await toolbox.api.get(downloadFile, localConfigData.token)
        if (data.command !== undefined) {
          for (i in data.command) {
            if (choosenVersions[i] !== '') {
              data.command[i] += `@${choosenVersions[i]}`
              print.error(
                `${choosenPackages[i]} version: ${choosenVersions[i]}`
              )
            }
            await toolbox.action.exec(projectName, data.command[i], false)
          }
        }

        /* istanbul ignore next */
        for (i in data.url) {
          if (data.url[i] !== undefined) {
            const downloadJson = await toolbox.api.get(
              data.url[i],
              localConfigData.token
            )
            if (downloadJson === false) {
              print.error('Could not download json file.')
              undoSpinner = print.spin('Undoing what we have done...')
              await toolbox.action.undo(projectName)
              undoSpinner.succeed('Just deleted everything :)')
              process.exit(1) // exit with error
            }
            await filesystem.file(`./${projectName}/${data.name[i]}`)
            await filesystem.write(
              `./${projectName}/${data.name[i]}`,
              downloadJson
            )
          }
        }
        /* istanbul ignore next */
        for (i in data.fileToEdit) {
          for (j in data.editAction[i]) {
            /* istanbul ignore next */
            if (data.editAction[i][j].type === 'add') {
              /* istanbul ignore next */
              await toolbox.action.add(
                projectName,
                data.fileToEdit[i],
                data.editAction[i][j].content
              )
              /* istanbul ignore next */
            } else if (data.editAction[i][j].type === 'delete') {
              /* istanbul ignore next */
              await toolbox.action.remove(
                projectName,
                data.fileToEdit[i],
                data.editAction[i][j].content
              )
              /* istanbul ignore next */
            } else if (data.editAction[i][j].type === 'replace') {
              /* istanbul ignore next */
              await toolbox.action.replace(
                projectName,
                data.fileToEdit[i],
                data.editAction[i][j].content,
                data.editAction[i][j].newContent
              )
            }
          }
        }
      }
      /* istanbul ignore next */
      print.warning(`type: cd ${projectName}`)
      /* istanbul ignore next */
      print.success('Have a good work :)')
      /* istanbul ignore next */
      process.exit(0)
    }
  }
}
