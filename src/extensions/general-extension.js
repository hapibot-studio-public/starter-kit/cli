/* eslint-disable no-param-reassign */
let jsonPkg
module.exports = toolbox => {
  const { filesystem } = toolbox
  toolbox.general = {
    isJson: async str => {
      try {
        JSON.parse(str)
      } catch (e) {
        return false
      }
      return true
    },
    replaceAll: async (content, toReplace, replaceWith) =>
      content.split(toReplace).join(replaceWith),
    setProjectName: async projectName => {
      jsonPkg = await filesystem.read(`./${projectName}/package.json`, 'json')
      jsonPkg.name = projectName.toString()
      await filesystem.write(`./${projectName}/package.json`, jsonPkg)
    }
  }
}
