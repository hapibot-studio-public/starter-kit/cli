/* eslint-disable no-param-reassign */
module.exports = async toolbox => {
  const { prompt } = toolbox
  toolbox.question = {
    input: async msg => {
      const input = {
        type: 'input',
        name: 'URL',
        message: msg
      }
      const q = [input]
      const answer = await prompt.ask(q)
      return answer.URL
    },

    select: async names => {
      const template = {
        type: 'select',
        name: 'selected',
        message: 'Choose template',
        choices: names
      }
      const question = [template]
      const choosenTemplate = await prompt.ask(question)

      return choosenTemplate.selected
    },

    sure: async () => {
      const sure = {
        type: 'select',
        name: 'selected',
        message: 'Are you sure?',
        choices: ['Yes', 'No']
      }
      const q = [sure]
      const answer = await prompt.ask(q)
      return answer.selected
    },

    checkBoxQuestion: async list => {
      const checkbox = {
        type: 'checkbox',
        name: 'check',
        message: 'Select those you want!',
        choices: list
      }

      const q = [checkbox]
      const answer = await prompt.ask(q)
      return answer.check
    },
    confirm: async msg => {
      const confirm = {
        type: 'confirm',
        name: 'confirmation',
        message: msg,
        choices: ['Y', 'N']
      }
      const q = [confirm]
      const answer = await prompt.ask(q)
      return answer.confirmation
    }
  }
}
