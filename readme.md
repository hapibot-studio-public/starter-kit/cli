# Create-Hapibot-App

![pipeline](https://gitlab.com/hapibot-studio-public/starter-kit/cli/badges/master/pipeline.svg)
[![codecov](https://codecov.io/gl/hapibot-studio:cli/cli/branch/master/graph/badge.svg?token=bwQLFxh0H7)](https://codecov.io/gl/hapibot-studio:cli/cli)

**create-hapibot-app** is a command-line application that helps you create a new project based on a template made by you.

**Features**

> Start a project based on your template within a few seconds

> Have general packages that can be used by all templates (optional)

> Supports **Linux**, **Windows** and **MACOS**

> You're able to choose a specific version of the general packages for each template

# Getting Started

Just install `create-hapibot-app` like this:

```
npm install -g create-hapibot-app
```

## It's necessary to do a few basic steps before using it, run:

```
create-hapibot-app config
```

- You will need to get your group id
- You will be asked for URL in this format: `https://gitlab.com/api/v4/groups/GROUPID/projects`, this URL is a link for a group of projects(templates). You may use this as an example:

```
https://gitlab.com/api/v4/groups/6454448/projects
```

- Next, you will be asked for a token this is needed if your projects are private [Personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
- If you are not using any private template you may leave this empty.

- Next, you will be asked for a repo id, this repo contains some `JSON` files to use as configuration to extra packages. For example:
  You have a template of a react-app with some default packages in it. But in some projects you would like to install [storybook](https://storybook.js.org/) then when you are creating your project, you will be asked if you want to install it.

You can use this as an example:

```
15578962
```

## Now you can start creating some apps:

Just type:

```
create-hapibot-app myapp
```

<img src="https://i.imgur.com/D2CFcAQ.png" align="center" alt= "Example of usage" styple="float: left; margin-right 10px;">

# Architecture

**MACOS**

Config file location: ~/.hapi-cli/config.json

**Linux**

Config file location: ~/.hapi-cli/config.json

**Windows**

Config file location: _to be defined_

**config.json Example:**

```
{
 "url": "https://gitlab.com/api/v4/groups/GROUPID/projects",
 "token": "<TOKEN>",
 "id": "<ID>"
}
```

# starter kits

```
Your templates must have a config.json file, with info about the programming language
that template is used for, a list of extra package names that it supports and a project description

```

**config.json file:**

```
{
 "Language": "Programming language for this starter kit",
 "packages": ["List of Package names that can be used with this starter kit"]
 "Description": "Project Description"
}
```

# packages

If you listed some extra packages in the template config file, now you need to upload a config file for each package. Those can be used for any template later, as long as you name them inside its config file.
Installation of packages can be done downloading files kept in URL, running commands and edit files, adding, replacing or removing data.

**Config.json file:**

```

{
 "name": ["fileNameListToDownload"],
 "command": ["cmdListToInstall"],
 "url": ["urlListToDownload"],
 "fileToEdit" : ["listOfFilesToEdit"],
 "editAction" : [[{"type": "add", "content" : "content to add to the file"}]]
}
```

# License

MIT - see LICENSE
