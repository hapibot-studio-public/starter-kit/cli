/* eslint-disable no-undef */
const stdin = require('mock-stdin')

const { filesystem } = require('gluegun')

const Tool = require('../src/cli').run('--build-test')

// helper function for timing
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

let io = null
beforeAll(() => {
  io = stdin.stdin()
})
afterAll(() => {
  io.restore()
})

function saveConfigFile() {
  const from = `${filesystem.homedir()}/.create-hapibot-app/config.json`
  const to = `${filesystem.homedir()}/.create-hapibot-app/config.json.bak`
  filesystem.remove(to)
  filesystem.copy(from, to)
  filesystem.remove(from)
  filesystem.file(from)
}

async function testId() {
  const from = `${filesystem.homedir()}/.create-hapibot-app/config.json`

  const data = await filesystem.read(from, 'json')
  const jsonConfig = {
    url: data.url,
    token: data.token,
    id: undefined
  }
  await saveConfigFile()
  const path = `${filesystem.homedir()}/.create-hapibot-app/config.json`
  filesystem.write(path, jsonConfig)
}

function restoreConfigFile() {
  const from = `${filesystem.homedir()}/.create-hapibot-app/config.json.bak`
  const to = `${filesystem.homedir()}/.create-hapibot-app/config.json`
  filesystem.remove(to)
  filesystem.copy(from, to)
  filesystem.remove(from)
}

const keys = {
  up: '\x1B\x5B\x41',
  down: '\x1B\x5B\x42',
  enter: '\x0D',
  space: '\x20'
}

describe('verifications-test', () => {
  test('git', async () => {
    const output = await Tool.then(toolbox => toolbox.verify.git())
    expect(output).toEqual(true)
  })

  test('connection', async () => {
    const output = await Tool.then(toolbox => toolbox.verify.connection())
    expect(output).toEqual(true)
  })

  test('folderExists', async () => {
    const output = await Tool.then(toolbox =>
      toolbox.verify.folderExists('__tests__')
    )
    expect(output).toEqual(true)
  })

  test('folder do not exists', async () => {
    const output = await Tool.then(toolbox =>
      toolbox.verify.folderExists('__doNotExist__')
    )
    expect(output).toEqual(false)
  })

  test('file exists', async () => {
    await filesystem.file('someFileText.txt')
    const output = await Tool.then(toolbox =>
      toolbox.verify.folderExists('someFileText.txt')
    )
    expect(output).toEqual(true)
    await filesystem.remove('someFileText.txt')
  })

  test('hasUrl', async () => {
    saveConfigFile()
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('URL')
        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 2)
      return toolbox.verify.hasUrl()
    })
    restoreConfigFile()
    expect(output).toEqual(0)
  })

  test('hasUrl and is empty space', async () => {
    saveConfigFile()
    const someConfigExample = {
      url: '',
      token: '',
      id: '15307435'
    }
    const path = `${filesystem.homedir()}/.create-hapibot-app/config.json`
    await filesystem.write(path, someConfigExample)
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('https://gitlab.com/api/v4/groups/6454448/projects')
        await delay(15)
        io.send(keys.enter)
        await delay(15)
      }
      setTimeout(() => sendKeystrokes().then(), 50)
      return toolbox.verify.hasUrl()
    })
    await filesystem.remove(path)
    restoreConfigFile()
    expect(output).toEqual(true)
  })

  test('You has a URL', async () => {
    saveConfigFile()
    const someConfigExample = {
      url: 'https://gitlab.com/api/v4/groups/6454448/projects',
      token: '',
      id: '15307435'
    }
    const path = `${filesystem.homedir()}/.create-hapibot-app/config.json`
    await filesystem.write(path, someConfigExample)
    const output = await Tool.then(toolbox => {
      return toolbox.verify.hasUrl()
    })
    await filesystem.remove(path)
    restoreConfigFile()
    expect(output).toEqual(true)
  })

  test('hasId', async () => {
    await testId()
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('ID')
        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 2)
      return toolbox.verify.hasId()
    })
    restoreConfigFile()
    expect(output).toEqual(true)
  })

  test('hasToken', async () => {
    await testId()
    const someConfigExample = {
      url: 'https://gitlab.com/api/v4/groups/6454448/projects',
      token: '',
      id: '15307435'
    }
    const path = `${filesystem.homedir()}/.create-hapibot-app/config.json`
    await filesystem.write(path, someConfigExample)
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('Y')
        await delay(30)
        io.send('TOKEN')
        await delay(30)
        io.send(keys.enter)
        await delay(30)
      }
      setTimeout(() => sendKeystrokes().then(), 250)
      return toolbox.verify.hasToken()
    })
    await filesystem.remove(path)
    restoreConfigFile()
    expect(output).toEqual(true)
  })

  test('has no token test', async () => {
    await testId()
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('N')
        await delay(15)
      }
      setTimeout(() => sendKeystrokes().then(), 25)
      return toolbox.verify.hasToken()
    })
    restoreConfigFile()
    expect(output).toEqual(false)
  })
})
