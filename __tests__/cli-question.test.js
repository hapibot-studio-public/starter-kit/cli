/* eslint-disable no-undef */
const stdin = require('mock-stdin')
const Tool = require('../src/cli').run('--build-test')

let io = null
beforeAll(() => {
  io = stdin.stdin()
})
afterAll(() => {
  io.restore()
})

const keys = {
  up: '\x1B\x5B\x41',
  down: '\x1B\x5B\x42',
  enter: '\x0D',
  space: '\x20'
}
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

describe('questions-test', () => {
  test('input', async () => {
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('input')
        await delay(2)

        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 7)
      return toolbox.question.input('test')
    })
    expect(output).toContain('input')
  })

  test('select', async () => {
    const output = await Tool.then(toolbox => {
      const names = ['one', 'two', 'three']
      const sendKeystrokes = async () => {
        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 7)
      return toolbox.question.select(names)
    })
    expect(output).toContain('one')
  })

  test('sure', async () => {
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 7)
      return toolbox.question.sure()
    })
    expect(output).toContain('Yes')
  })

  test('checkBoxQuestion', async () => {
    const output = await Tool.then(toolbox => {
      const names = ['one', 'two', 'three']
      const sendKeystrokes = async () => {
        io.send(keys.space)
        io.send(keys.down)
        io.send(keys.space)
        io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 7)
      return toolbox.question.checkBoxQuestion(names)
    })
    expect(output.toString()).toContain(['one', 'two'].toString())
  })

  test('Confirmation', async () => {
    const output = await Tool.then(toolbox => {
      const sendKeystrokes = async () => {
        io.send('Y')
        // io.send(keys.enter)
      }
      setTimeout(() => sendKeystrokes().then(), 7)
      return toolbox.question.confirm('Ola')
    })
    expect(output).toEqual(true)
  })
})
