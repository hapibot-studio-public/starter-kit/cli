/* eslint-disable no-undef */
const stdin = require('mock-stdin')

const { system, filesystem, print } = require('gluegun')
const Tool = require('../src/cli').run('--build-test')
let config
let expected
// Key codes
const keys = {
  up: '\x1B\x5B\x41',
  down: '\x1B\x5B\x42',
  enter: '\x0D',
  space: '\x20'
}

// Mock stdin so we can send messages to the CLI
let io = null
beforeAll(() => {
  io = stdin.stdin()
  jest.setTimeout(20000)
})
afterAll(() => {
  io.restore()
})

// helper function for timing
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

function saveConfigFile() {
  const from = `${filesystem.homedir()}/.create-hapibot-app/config.json`
  const to = `${filesystem.homedir()}/.create-hapibot-app/config.json.bak`
  filesystem.remove(to)
  filesystem.copy(from, to)
}

function restoreConfigFile() {
  const from = `${filesystem.homedir()}/.create-hapibot-app/config.json.bak`
  const to = `${filesystem.homedir()}/.create-hapibot-app/config.json`
  filesystem.remove(to)
  filesystem.copy(from, to)
  filesystem.remove(from)
}

const src = filesystem.path(__dirname, '..')

const cli = async cmd =>
  system.run(`node ${filesystem.path(src, 'bin', 'create-hapibot-app')} ${cmd}`)

test('outputs version', async () => {
  const output = await cli('--version')
  expect(output).toContain('0.1.5')
})

test('outputs help', async () => {
  const output = await cli('--help')
  expect(output).toContain('0.1.5')
})

test('generates file', async () => {
  const output = await cli('generate foo')

  expect(output).toContain('Generated file at models/foo-model.js')
  const foomodel = filesystem.read('models/foo-model.js')

  expect(foomodel).toContain('module.exports = {')
  expect(foomodel).toContain("name: 'foo'")

  // cleanup artifact
  filesystem.remove('models')
})

describe('list templates', () => {
  test('without config param', async () => {
    // const output = await cli('list')
    await require('../src/cli').run('list')
    // expect(output).toEqual(output)
  })
})

describe('config file', () => {
  beforeEach(() => {
    saveConfigFile()
  })

  afterEach(() => {
    restoreConfigFile()
  })

  test('config file with param', async () => {
    await require('../src/cli').run(
      'config --url=https://example.com --token=BDA_123 --id=123456'
    )
    config = await filesystem.read(
      `${filesystem.homedir()}/.create-hapibot-app/config.json`,
      'json'
    )
    expected = { id: 123456, token: 'BDA_123', url: 'https://example.com' }
    expect(config).toEqual(expected)
  })

  test('config file without param', async () => {
    const sendKeystrokes = async () => {
      //URL
      io.send('https://gitlab.com/api/v4/groups/6454448/projects')
      io.send(keys.enter)
      await delay(5)
      // token
      io.send('')
      io.send(keys.enter)
      await delay(5)
      //id
      io.send('15307435')
      io.send(keys.enter)
      await delay(5)
    }
    setTimeout(() => sendKeystrokes().then(), 10)
    await require('../src/cli').run('config')
    // return toolbox
    // return toolbox.question.input('test')

    config = await filesystem.read(
      `${filesystem.homedir()}/.create-hapibot-app/config.json`,
      'json'
    )
    // console.log(config)
    expected = {
      url: 'https://gitlab.com/api/v4/groups/6454448/projects',
      token: '',
      id: '15307435'
    }
    expect(config).toEqual(expected)
  })
})

// describe('creating a project', () => {
//   test('without prj name, without any extra pkgs', async () => {
//     await filesystem.remove('__TestingApp__')
//     const sendKeystrokes = async () => {
//       //PRJ name
//       io.send('__TestingApp__')
//       // io.send(keys.enter)
//       await delay(15)
//       // select template
//       io.send(keys.down)
//       io.send(keys.enter)
//       await delay(50)
//       //are u sure
//       io.send(keys.enter)
//       await delay(500)
//       //select extra pkgs
//       io.send(keys.enter)
//       // await delay()
//       // io.send(keys.enter)
//       // await delay(15)
//     }
//     setTimeout(() => sendKeystrokes().then(), 3000)
//     await require('../src/cli').run('')
//     // return toolbox
//     // return toolbox.question.input('test')

//     await filesystem.remove('./__TestingApp__')
//     expect(config).toEqual(expected)
//   })
// })
