/* eslint-disable no-undef */
const { filesystem } = require('gluegun')

const Tool = require('../src/cli').run('--build-test')

test('action exec', async () => {
  await filesystem.dir('testInitGit')

  const output = await Tool.then(toolbox =>
    toolbox.action.exec('testInitGit', 'git init', true)
  )
  expect(output).toEqual(0)
})

test('action clone', async () => {
  const output = await Tool.then(toolbox =>
    toolbox.action.clone(
      'https://gitlab.com/gpguia/clone-test.git',
      'gitTestCloneRepo',
      true
    )
  )
  expect(output).toEqual(0)
  filesystem.remove('./gitTestCloneRepo')
})

test('action setup git', async () => {
  const output = await Tool.then(toolbox =>
    toolbox.action.setupGit('testInitGit', true)
  )
  expect(output).toEqual(0)
})

test('action as append', async () => {
  await filesystem.file('./testInitGit/actionAdd.txt')
  const output = await Tool.then(toolbox =>
    toolbox.action.add('testInitGit', 'actionAdd.txt', 'Text')
  )
  // const content = await filesystem.read('./testInitGit/actionAdd.txt')
  expect(output).toEqual('append')
})

test('action remove', async () => {
  await filesystem.file('./testInitGit/actionAdd.txt')
  await Tool.then(toolbox =>
    toolbox.action.remove('testInitGit', 'actionAdd.txt', 'Text')
  )
  const fileContent = await filesystem.read('./testInitGit/actionAdd.txt')
  expect(fileContent).toEqual('')
})

test('action replace', async () => {
  await filesystem.file('./testInitGit/actionAdd.txt')
  await Tool.then(toolbox =>
    toolbox.action.replace('testInitGit', 'actionAdd.txt', '', 'unit test')
  )
  const fileContent = await filesystem.read('./testInitGit/actionAdd.txt')
  expect(fileContent).toEqual('unit test')
})

test('action undo', async () => {
  await Tool.then(toolbox => toolbox.action.undo('testInitGit'))
  const exists = await filesystem.exists('./testInitGit')
  expect(exists).toEqual(false)
})
